#!/bin/env python3
#
# plot to screen
#
# ./plot.py 2022-ephemerides.csv.gz
#

import matplotlib.pyplot as plt
import datetime
import re
import csv
import math
import numpy as np
import sys
import argparse
import gzip
import copy
import cairo
from collections import defaultdict
from scipy import optimize

GNOMON = (0, 0) # left bottom
H = 10 # gnomon in metters
WW = 70 # half field width in metters
HH = 70 # field height in metters
XX1 = -WW
XX2 = WW
XX = XX2 - XX1 # 2*WW
YY1 = -5 # BOTTOM
YY2 = YY1 + HH # TOP
YY = YY2 - YY1 # HH
VIEWPORT = (XX1, XX2, YY1, YY2) # plot viewport

LEFT = [(XX1,YY1),(XX1,YY2)]
RIGHT = [(XX2,YY1),(XX2,YY2)]
BOTTOM = [(XX1,YY1),(XX2,YY1)]
BOTTOM10 = [(XX1,YY1),(0,YY1)]
BOTTOM01 = [(0,YY1),(XX2,YY1)]
TOP = [(XX1,YY2),(XX2,YY2)]
TOP10 = [(XX1,YY2),(0,YY2)]
TOP01 = [(0,YY2),(XX2,YY2)]

# cairo has [0,0] at left, top
WIDTH = 7000
HEIGHT = int(WIDTH * (YY/float(XX))) # A4 = 210 x 297
SX = WIDTH / float(HEIGHT)
SY = 1.0
OX = SX/2 # cairo offset x
OY = SY # cairo offset y

surface = cairo.ImageSurface(cairo.FORMAT_ARGB32, WIDTH, HEIGHT)
ctx = cairo.Context(surface)
ctx.scale(HEIGHT, HEIGHT) # normalizing the canvas

def parse_date(d):
  return datetime.datetime.strptime(d, '%Y-%m-%d %H:%M:%S')

def dms2dd(degrees, minutes, seconds, direction):
    d = float(degrees)
    dd = d + float(minutes)/60 + float(seconds)/(60*60);
    if degrees.startswith('-') and d == 0:
      dd *= -1
    if direction == 'E' or direction == 'N':
      dd *= -1
    return dd;

def dd2dms(deg):
    d = int(deg)
    md = abs(deg - d) * 60
    m = int(md)
    sd = (md - m) * 60
    return [d, m, sd]

def parse_dms(dms):
    parts = re.split('[°\'"]+', dms)
    lat = dms2dd(parts[0], parts[1], parts[2], parts[3])
    return (lat)

def line_intersect(Ax1, Ay1, Ax2, Ay2, Bx1, By1, Bx2, By2):
    """ returns a (x, y) tuple or None if there is no intersection """
    d = (By2 - By1) * (Ax2 - Ax1) - (Bx2 - Bx1) * (Ay2 - Ay1)
    if d:
        uA = ((Bx2 - Bx1) * (Ay1 - By1) - (By2 - By1) * (Ax1 - Bx1)) / d
        uB = ((Ax2 - Ax1) * (Ay1 - By1) - (Ay2 - Ay1) * (Ax1 - Bx1)) / d
    else:
        return False
    if not(0 <= uA <= 1 and 0 <= uB <= 1):
        return False
    x = Ax1 + uA * (Ax2 - Ax1)
    y = Ay1 + uA * (Ay2 - Ay1) 
    return x, y

def list_intersection(L1, L2):
  for i in range(0, len(L2)-1):
    a, b = L1
    c, d = L2[i:i+2]
    r = line_intersect(*a, *b, *c, *d)
    if r:
      return r
  return False

def lists_intersection(L1, L2):
  for i in range(0, len(L1)-1):
    xx = L1[i:i+2]
    xy = list_intersection(xx, L2)
    if xy:
      return xy
  return False

def plot(a, d):
  if not a:
    return
  print(a)
  x, y = zip(*a)
  plt.plot(x, y)
  OFF = -100
  xy = list_intersection(LEFT, a)
  print(f'xy={xy} {LEFT}')
  if not xy:
    xy = list_intersection(BOTTOM10, a)
  if not xy:
    xy = list_intersection(BOTTOM01, a)
    OFF = 30
  if not xy:
    xy = list_intersection(RIGHT, a)
    OFF = 30
  if not xy:
    xy = a[0]
  print(f'intersection {xy}')
  if xy and d != None:
    plt.annotate(d, xy=xy, xycoords='data', xytext=(OFF, 30), textcoords='offset points', arrowprops=dict(arrowstyle="->"))

def minmax(p):
  pmin = sys.maxsize
  pmax = 0
  for (x,y) in p:
    if pmin > y:
      pmin = y
    if pmax < y:
      pmax = y
  left = list_intersection(LEFT, p)
  right = list_intersection(RIGHT, p)
  if left and right:
    ymin = min(left[1], right[1])
    ymax = max(left[1], right[1])
    if pmax > YY1 and pmax < YY2:
      return (ymin, pmax)
    if pmin > YY1 and pmin < YY2:
      return (pmin, ymax)
  xy = list_intersection(TOP, p)
  if xy:
    return (xy[1], pmax)
  else:
    xy = list_intersection(BOTTOM, p)
  if xy:
    return (pmin, xy[1])
  return None

def get_angle(point_1, point_2):
  return math.atan2(point_1[1] - point_2[1], point_1[0] - point_2[0])

def distance(p1, p2):
  return math.sqrt((p1[0]-p2[0])**2 + (p1[1]-p2[1])**2)

def plot_day(a, d, SML, SMR):
  x, y = zip(*a)
  plt.plot(x, y)
  xy = None
  if SMR:
    OFFX=-40
    OFFY=30
    xy = list_intersection(LEFT, a)
    if not xy:
      xy = list_intersection(TOP10, a)
    if not xy:
      xy = list_intersection(BOTTOM10, a)
      OFFY=-30
  if SML:
    OFFX=30
    OFFY=30
    xy = list_intersection(RIGHT, a)
    if not xy:
      xy = list_intersection(TOP01, a)
      OFFY=30
    if not xy:
      xy = list_intersection(BOTTOM01, a)
      OFFY=-30
  if xy:
    plt.annotate(d.strftime("%d %b"), xy=xy, xycoords='data', xytext=(OFFX, OFFY), textcoords='offset points', arrowprops=dict(arrowstyle="->"))
  cairo_day(a, d, SML, SMR)

def plot_hour(x, h, xy):
  plot(x, None)
  angle = get_angle([0, 0], xy)
  LEN = 10
  # LEN = 100 * (0.09 + math.fabs((angle + math.pi/2) / (2*math.pi)))
  xoff = LEN * math.cos(angle)
  yoff = LEN * math.sin(angle)
  # plt.annotate("%s" % h, xy=xy, xycoords='data', xytext=(xoff, yoff), textcoords='offset points', arrowprops=dict(arrowstyle="->"))
  plt.annotate("%s" % h, xy=xy, xycoords='data', xytext=(xoff, yoff), textcoords='offset points')
  cairo_hour(x, h, xy)

def plot_gnomon(x, y):
  plt.scatter(0, 0)
  plt.annotate("POLE (" + str(H) + "m)", xy=(0, 0), xycoords='data', xytext=(-50, -20), textcoords='offset points', arrowprops=dict(arrowstyle="->"))
  cairo_gnomon(x,y)

def plotd(d):
  return True

#
# cairo
#

def ftext(): # set text font
  ctx.set_source_rgb(0.0, 0.0, 0.0)
  ctx.select_font_face("Cairo", cairo.FONT_SLANT_NORMAL, cairo.FONT_WEIGHT_NORMAL)
  ctx.set_font_size(0.01)

def ctext(t): # text size
  return ctx.text_extents(t) # (x1, y1, width, height, dx, dy)

def text(x, y, t): # print text
  ftext()
  (x1, y1, width, height, dx, dy) = ctext(t)
  ctx.move_to(x, y)
  ctx.show_text(t)
  return [x, y, width, height]

def textc(x, y, t): # text centered
  ftext()
  (x1, y1, width, height, dx, dy) = ctext(t)
  ctx.move_to(x - width/2, y + height/2)
  ctx.show_text(t)

def multiline(x, y, t):
  ftext()
  tt = t.split('\n')
  ww = 0
  hh = 0
  for k in tt:
    (x1, y1, width, height, dx, dy) = ctext(k)
    ww = max(ww, width)
    hh += height
  py = y - hh/2;
  for k in tt:
    (x1, y1, width, height, dx, dy) = ctext(k)
    ctx.move_to(x - ww/2, py)
    ctx.show_text(k)
    py += height

def multilinej(x, y, t): # justify
  ftext()
  tt = t.split('\n')
  ww = 0
  hh = 0
  for k in tt:
    (x1, y1, width, height, dx, dy) = ctext(k)
    ww = max(ww, width)
    hh += height
  py = y;
  for k in tt:
    (x1, y1, width, height, dx, dy) = ctext(k)
    ctx.move_to(x - ww/2 + (ww-width)/2, py)
    ctx.show_text(k)
    py += height

def clear():
  ctx.save()
  ctx.set_source_rgba(0.0, 0.0, 0.0, 0.0)
  ctx.set_operator(cairo.OPERATOR_SOURCE)
  ctx.paint()
  ctx.restore()

def cairo_xy(xy):
  return (OX + xy[0] / XX * SX, OY-(xy[1]-YY1) / YY)

def cairo_gnomon(x,y):
  xy = cairo_xy((x,y))
  ctx.arc(xy[0], xy[1], 0.005, 0, 2*math.pi)
  ctx.fill()
  multilinej(xy[0], xy[1] + 0.005*3, "Gnomon\n%dm" % H)

def cairo_day(xx, d, SML, SMR):
  tt = d.strftime("%d %b")
  xy = None
  if SMR:
    OFFX=0.02
    OFFY=0
    xy = list_intersection(LEFT, xx)
    if not xy:
      xy = list_intersection(TOP10, xx)
      OFFX=0
      OFFY=0.02
    if not xy:
      xy = list_intersection(BOTTOM10, xx)
      OFFX=0
      OFFY=-0.02
  if SML:
    OFFX=-0.02
    OFFY=0
    xy = list_intersection(RIGHT, xx)
    if not xy:
      xy = list_intersection(TOP01, xx)
      OFFX=0
      OFFY=0.02
    if not xy:
      xy = list_intersection(BOTTOM01, xx)
      OFFX=0
      OFFY=-0.02
  if xy:
    xy = cairo_xy(xy)
    textc(xy[0]+OFFX, xy[1]+OFFY, tt)
  aa = []
  for hh in xx:
    aa += [ cairo_xy(hh) ]
  h0 = aa[0]
  ctx.move_to(h0[0], h0[1])
  for hh in aa[1:]:
    ctx.line_to(hh[0], hh[1])
  ctx.set_source_rgba(0.0, 0.0, 0.0, 1)
  ctx.set_line_width(0.0005)
  ctx.stroke()

def cairo_hour(dd, h, xy):
  # mh - lowest position on screen for every hour to draw text mh[15] = [x, y]
  angle = get_angle(xy, [0, -5])
  LEN = -0.02
  xoff = LEN * math.cos(angle)
  yoff = LEN * math.sin(angle)
  xy = cairo_xy(xy)
  mh = {}
  first = True
  for hh in dd:
    hh = cairo_xy(hh)
    if first:
        ctx.move_to(hh[0], hh[1])
        first = False
    else:
        ctx.line_to(hh[0], hh[1])
    if not h in mh or mh[h][1] < hh[1]:
      mh[h] = hh
  ctx.set_source_rgba(0.0, 0.0, 0.0, 1)
  ctx.set_line_width(0.0005)
  ctx.stroke()
  for h in mh:
    hh = mh[h]
    textc(xy[0]+xoff, xy[1]-yoff, str(h))

clear()

parser = argparse.ArgumentParser(description='Optional app description')
parser.add_argument('-f','--from_date', type=str,help='from date')
parser.add_argument('-t','--to_date', type=str,nargs='?',help='to date')
parser.add_argument('-s','--step', type=str,nargs='?',help='step in days')
parser.add_argument('-p','--png', type=str,nargs='?',help='write results to png')
parser.add_argument('file', type=str,nargs='*',help='command to run')
args = parser.parse_args()

if args.from_date:
  args.from_date = parse_date(args.from_date)
if args.to_date:
  args.to_date = parse_date(args.to_date)

for FILE in args.file:
  with gzip.open(FILE, mode='rt') as csv_file:
    ff = csv_file.readline().strip()
    ff = ff.replace(", ", ",")
    ff = [ '{}'.format(x) for x in list(csv.reader([ff], delimiter=',', quotechar='"'))[0] ]
    csv_reader = csv.DictReader(csv_file, ff)
    line_count = 0
    day = None # currently reading day number 1-31
    PLOT = None
    PLOT_OLD = None
    SDM_OLD = None # SDM prev value
    SM_OLD = 0 # SM prev value (angle)
    SM = 0 # Sun Day Max (angle)
    SDM = None # Sun Day (while SM > 0)
    SMR = None # SM raising
    SML = None # SM lowering
    SMRM = 0 # sun angle at SMR; Солнцестояние
    YEARMIN = None # plot data for SMRM
    YEARMIND = None
    YEARMAX = None # plot data for SMRM
    SMLM = 0 # sun angle at SML
    YMIN = sys.maxsize
    YMAX = 0
    YD_OLD = None # Y day old value
    YD = None # Y Day; Equinox (Равноденствие)
    YDM = sys.maxsize # Y Day diff minimum
    YD_PLOT = None
    YDL = False
    HOUR = None
    HOURS = {} # hours plot (x,y)
    for row in csv_reader:
      for (k, v) in row.items():
        row[k.strip()] = row[k]
      d = row["Date and Time"].strip()
      d = parse_date(d)
      if args.from_date:
        if d < args.from_date:
          continue
      if args.to_date:
        if d > args.to_date:
          continue
      if args.step:
        if d.timetuple().tm_yday % int(args.step) != 0:
          continue
      A = row["Azimuth"].strip()
      A = parse_dms(A)
      R = math.radians(270 - A)
      a = row["Altitude"].strip()
      a = parse_dms(a)
      S = H / math.tan(math.radians(a)) # shadow length
      X = S * math.cos(R)
      Y = S * math.sin(R)
      if day != d.day:
        day = d.day
        if SDM:
          YMIN, YMAX = minmax(PLOT)
          print(f'{SDM} SM={SM} YMIN={YMIN} YMAX={YMAX} D={YMAX-YMIN}  HSTART={HSTART.time()} HEND={HEND.time()} HDIFF={HEND-HSTART}')
          if SDM_OLD:
            if SM_OLD < SM:
              if not SMR and SML:
                SMLM = SM_OLD
                YEARMIN = PLOT_OLD
                YEARMIND = SDM_OLD
                YDL = YD
                print(f'{SDM_OLD} YEAR MIN, DAY MAX {SM_OLD} YD={YD} YDM={YDM}')
                if YD_PLOT:
                  plot_day(YD_PLOT, YD, True, False)
                SML = False
                YD = None
                YDM = sys.maxsize
                YD_PLOT = None
              SMR = True
              SML = False
            if SM_OLD > SM:
              if SMR and not SML:
                SMRM = SM
                YEARMAX = PLOT_OLD
                YEARMAXD = SDM_OLD
                YDR = YD
                print(f'{SDM_OLD} YEAR MAX, DAY MAX {SM_OLD} YD={YD} YDM={YDM}')
                if YD_PLOT:
                  plot_day(YD_PLOT, YD, False, True)
                SMR = False
                YD = None
                YDM = sys.maxsize
                YD_PLOT = None
              SML = True
              SMR = False
          D = YMAX - YMIN
          if YDM > D: # Y lowering?
            YD = SDM
            YDM = D
            YD_PLOT = PLOT
        if SDM_OLD and PLOT_OLD and ( plotd(SDM_OLD) or YEARMAX == PLOT_OLD or YEARMIN == PLOT_OLD ):
          if SMR_OLD is None: # handing first day of a file
            SMR_OLD = SMR
          if SML_OLD is None: # handing first day of a file
            SML_OLD = SML
          plot_day(PLOT_OLD, SDM_OLD, SML_OLD, SMR_OLD)
          if PLOT_OLD == YD_PLOT:
            YD_PLOT = None # already printed
        PLOT_OLD = PLOT
        SM_OLD = SM
        SDM_OLD = SDM
        SMR_OLD = SMR
        SML_OLD = SML
        PLOT = []
        SM = 0
        SDM = None
        YMIN = sys.maxsize
        YMAX = 0
        HSTART = None
        HEND = None
      if a > 0:
        dold = d
        if not HSTART:
          HSTART = d
        PLOT += [(X, Y)]
        print(f'{d} S={S} azim={A} alt={a}')
        if SM < a:
          SM = a
          SDM = d
        h = d.hour
        if HOUR != h:
          HOUR = h
          k = HOURS.get(h,[])
          k.append((X,Y))
          HOURS[h] = k
      elif HSTART: # start day already defined
        HEND = dold
    print(f'Processed {line_count} lines.')

if SDM:
  print(f'{SDM} SM={SM}')

if False: # draw rest of data
  plot(PLOT, None) # PLOT_OLD and PLOT

for h in HOURS.keys():
  print (f'{h}={HOURS[h]}')
  min = sys.maxsize
  p = None
  for (x,y) in HOURS[h]:
    l = distance([0,0], (x,y))
    if l < min:
      min = l
      p = (x,y)
  print("h=" + str(p))
  plot_hour(HOURS[h], h, p)

plot_gnomon(0,0)

plt.xlim(XX1,XX2)
plt.ylim(YY1,YY2)
plt.yticks([])
if args.png:
  textc(SX/2, 0.01, str(SDM.year))
  multilinej(SX/2+0.25, 0.01, YEARMIND.strftime("%d %b") + "\n" + "min")
  multilinej(SX/2-0.25, 0.01, (YEARMIND+datetime.timedelta(days=1)).strftime("%d %b") + "\n" + "raising")
  multilinej(SX/2-0.2, SY - 0.02, YEARMAXD.strftime("%d %b") + "\n" + "max")
  multilinej(SX/2+0.2, SY - 0.02, (YEARMAXD+datetime.timedelta(days=1)).strftime("%d %b") + "\n" + "lowering")
  multilinej(SX/2-0.15, 0.1, (YDR).strftime("%d %b") + "\n" + "equinox")
  multilinej(SX/2+0.15, 0.1, (YDL).strftime("%d %b") + "\n" + "equinox")
  surface.write_to_png(args.png)
else:
  plt.show()