# Equinox

Equinox calucation based on data from Stellarium.

## Export Data

Start Steallarium. Goto 'Astronomical calculations window/Ephemeris'.

Select From Celestial body select 'Sun'. Set whole year in a Date 'from' 'to' fields.

Time step '10 minutes'. Then click 'Calculate ephemeris' then 'Export ephemeris' as 'csv' file. 

## Pictures

![+](/example-short.png)

![+](/example.png)
